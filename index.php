<?php
ini_set('display_errors', 1);

//Simple autoload function for the controllers, models and views
function simpleAutoload($class) {
	if (preg_match('/[a-zA-Z]+Controller/', $class)) {
		require_once __DIR__ . '/controllers/' . $class . '.php';
		return true;
	}
	if (preg_match('/[a-zA-Z]+Model/', $class)) {
		require_once __DIR__ . '/models/' . $class . '.php';
		return true;
	}
	if (preg_match('/[a-zA-Z]+View/', $class)) {
		require_once __DIR__ . '/views/' . $class . '.php';
		return true;
	}
}


// register the autoloader
spl_autoload_register('simpleAutoload');


$controller = ucfirst($_GET['controller']);
$action = $_GET['action'];
$actionview = ucfirst($controller . $action . "View");
// append Controller and Action to the params

$controller = $controller . "Controller";
$action  = $action . "Action";



$controllerClass = new $controller;
$response = $controllerClass->$action();
$view = new $actionview();
$view->render($response);